fn main() {
    let x = " ";
    let x = x.len();
    let y = 98;

    println!("Hello, world! {}", x);
    println!("Hello, function! {}", return_from_loop(1000));
    println!("Hello, function! {}", return_from_loop(y));
}

fn return_from_loop(mut x: i32) -> i32 {
    loop {
        x -= 5;
        if x < 5 {
            break x + 1;
        }
    }
}
