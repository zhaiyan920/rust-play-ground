#[cfg(test)]
#[cfg(unix)]
mod inner {

    use std::sync::mpsc;

    use std::net::{SocketAddr, TcpListener, TcpStream};
    use std::process::{Command, Stdio};
    use std::sync::{Arc, Condvar, Mutex};
    use std::time::Duration;

    fn sudo(args: &[&str]) -> String {
        let child = Command::new("sudo")
            .args(args)
            .stdout(Stdio::piped())
            .spawn()
            .expect("failed to execute process");

        let counter = Arc::new((Mutex::new(false), Condvar::new()));
        let counter2 = counter.clone();
        let (sender, receiver) = mpsc::channel();

        std::thread::spawn(move || {
            let output = child
                .wait_with_output()
                .expect("child execute correctly")
                .stdout;
            let &(ref lock, ref cond) = &*counter2;
            let mut state = lock.lock().unwrap();
            *state = true;
            cond.notify_one();
            sender.send(output).unwrap();
        });

        // kill and panic if 2 sec later it still does not return.
        let &(ref lock, ref cond) = &*counter;
        let mut state = lock.lock().unwrap();
        loop {
            let result = cond.wait_timeout(state, Duration::from_secs(2)).unwrap();
            state = result.0;
            if *state {
                break;
            }
        }
        String::from_utf8(receiver.recv().unwrap()).unwrap()
    }

    fn save_sysctl_option() -> isize {
        let content = if cfg!(target_os = "linux") {
            sudo(&["sysctl", "net.ipv6.bindv6only"])
        } else if cfg!(target_os = "macos") {
            sudo(&["sysctl", "net.inet6.ip6.v6only"])
        } else {
            panic!("unsupported unix system");
        };
        println!("content: {}", content);
        let v = content
            .trim()
            .split(|c| c == '=' || c == ':')
            .collect::<Vec<&str>>();
        println!("v = {:?}", v);

        return content
            .trim()
            .split(|c| c == '=' || c == ':')
            .collect::<Vec<&str>>()[1]
            .trim()
            .parse::<isize>()
            .unwrap();
    }

    fn apply_sysctl_option(v: isize) {
        if cfg!(target_os = "linux") {
            sudo(&["sysctl", &format!("net.ipv6.bindv6only={}", v)]);
        } else if cfg!(target_os = "macos") {
            sudo(&["sysctl", &format!("net.inet6.ip6.v6only={}", v)]);
        } else {
            panic!("unsupported unix system");
        };
    }

    #[test]
    fn test_connection_with_both_v4_and_v6_when_v6only_is_global() {
        // We want to enable v4-v6 connection no matter the global option
        // about v6 socket behavior.
        let original_bind_v = save_sysctl_option();
        apply_sysctl_option(1);

        // Verify that bind on a socket will not accept v4 connection
        {
            let listener = TcpListener::bind("[::]:0").unwrap();
            let bind_addr = listener.local_addr().unwrap();
            std::thread::spawn(move || {
                for s in listener.incoming() {
                    let stream = s.unwrap();
                    println!("!!stream from {} connected\n", stream.local_addr().unwrap());
                }
            });
            let connect_addr = format!("127.0.0.1:{}", bind_addr.port())
                .parse::<SocketAddr>()
                .expect("valid bind address");

            TcpStream::connect_timeout(&connect_addr, Duration::from_secs(2))
                .expect_err("should not connect due to ipv6 only");
        }

        // Verify pipefitter still accept v4 connection
        {
            let pf_listener =
                pipefitter::std_tcp_listener_v4_v6(&"[::]:0".parse::<SocketAddr>().unwrap());
            let bind_addr = pf_listener.local_addr().unwrap();
            std::thread::spawn(move || {
                for s in pf_listener.incoming() {
                    let stream = s.unwrap();
                    println!(
                        "hi stream from {} connected\n",
                        stream.local_addr().unwrap()
                    );
                }
            });
            let connect_addr = format!("127.0.0.1:{}", bind_addr.port())
                .parse::<SocketAddr>()
                .expect("valid bind address");
            TcpStream::connect_timeout(&connect_addr, Duration::from_secs(2))
                .expect("should connect since we enabled it");
        }

        apply_sysctl_option(original_bind_v);
    }
}
