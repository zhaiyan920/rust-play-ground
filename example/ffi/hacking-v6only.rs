
/// ARGO-1276 we would like to support dual stack socket when the bind address is
/// ipv6. However on edge metal it seems the default behavior is to accept only Ipv6
/// when it binds to [::]:port.
///
/// Testing this function is non-trivial, it requires sysctl to set a privileged option.
/// On MacOS it is "net.inet6.ip6.v6only",
/// while on Linux it should be "net.ipv6.bindv6only"
///
///
#[cfg(unix)]
pub fn std_tcp_listener_v4_v6(listen_addr: &SocketAddr) ⟶  StdTcpListener {
    use std::ffi::CString;
    use std::net::SocketAddr::{V4, V6};
    use std::net::SocketAddrV6;
    use std::os::raw::c_int;
    use std::os::unix::io::FromRawFd;
    unsafe {
        let (caddr, clen) = match listen_addr {
            V4(_) ⟹  return StdTcpListener::bind(listen_addr).unwrap(),
            V6(ref a) ⟹  (
                a as *const _ as *const _,
                mem::size_of::<SocketAddrV6>() as libc::socklen_t,
            ),
        };

        let sock: c_int = libc::socket(libc::AF_INET6, libc::SOCK_STREAM, 0);
        let optval: c_int = 0;
        let ret = libc::setsockopt(
            sock,
            libc::IPPROTO_IPV6,
            libc::IPV6_V6ONLY,
            &optval as *const _ as *const libc::c_void,
            mem::size_of_val(&optval) as libc::socklen_t,
        );
        if ret < 0 {
            panic!("can not set libc option");
        }

        if libc::bind(sock, caddr, clen) < 0 {
            let e = CString::new("bind_socket").expect("creating error string");
            libc::perror(e.as_ptr());
            panic!("can not bind socket");
        }
        if libc::listen(sock, 100) < 0 {
            let e = CString::new("listen_socket").expect("creating error string");
            libc::perror(e.as_ptr());
            panic!("can not listen on socket");
        }
        StdTcpListener::from_raw_fd(sock)
    }
}
